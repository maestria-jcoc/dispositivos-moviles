#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>

using namespace std;

#define GPIO "/sys/class/gpio"
#define SLEEPTIME 1
#define BLINK_TIMES 5

class LED{
    private: 
        string path;
        int number;
        virtual void writeLED(string fileName, string value);
        virtual void setupLED();
    public:
        LED(int number);
        virtual void turnOn();
        virtual void turnOff();
        virtual void blink();
        virtual ~LED();
};

LED::LED(int number){
    this->number = number;
    ostringstream ledString;
    ledString << GPIO << "/gpio" << number;
    this->path = string(ledString.str());
}

void LED::writeLED(string fileName, string value) {
    ofstream fs;
    fs.open((this->path + fileName).c_str());
    fs << value;
    fs.close();
}

void LED::setupLED() {
    ofstream fs;

    fs.open((GPIO + string("/unexport")).c_str());
    fs << this->number;
    fs.close();

    fs.open((GPIO + string("/export")).c_str());
    fs << this->number;
    fs.close();
    
    writeLED("/direction", "out");
}

void LED::turnOn() {
    cout << "LED" << this->number << " on" << endl;
    setupLED();
    writeLED("/value", "1");
}

void LED::turnOff() {
    cout << "LED " << this->number << " off" << endl;
    setupLED();
    writeLED("/value", "0");
}

void LED::blink() {
    for(int i = 0; i < BLINK_TIMES; i++) {
        turnOn();
        sleep(SLEEPTIME);
        turnOff();
        sleep(SLEEPTIME);
    }
}

LED::~LED() {
    //ofstream fs;

    cout << "removing LED " << this->number << endl;
    
    //fs.open((GPIO + string("/unexport")).c_str());
    //fs << this->number;
    //fs.close();
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        cout << "Invalid Argument count" << endl;
        return -1;
    }

    cout << "starting LED App" << endl;

    string cmd = string(argv[1]);
    LED led1 = LED(14);

    if(cmd == "on") {
        led1.turnOn();
    } else if (cmd == "off") {
        led1.turnOff();
    } else if( cmd == "blink") {
        led1.blink();
    } else {
        cout << "Wrong command passed" << endl;
    }

    cout << "Script done " << endl;
    return 0;
}