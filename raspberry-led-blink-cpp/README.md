# Conexiones necesarias
    Se tiene que conectar un LED al puerto (8) GPIO14 
    y cualquier GND de la raspberry con una resistencia
    de al menos 330Ohms para evitar una sobrecarga al LED.

# Compilación
```
    g++ main.cpp -o main.o
```

# Modo de uso
```
    sudo ./main.o on
    sudo ./main.o off
    sudo ./main.o blink
```
