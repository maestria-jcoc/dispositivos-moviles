# Módulo Sensor de Temperatura DS18B20

## Descripción:
Este pequeño pero práctico módulo permite la medición de la temperatura ambiente utilizando el bus serie digital. Consta de un sensor de temperatura digital de bus único DS18B20, un LED y una resistencia. Este sensor es una opción útil y económica para proyectos de electrónica populares con tarjetas electrónicas como Arduino, Raspberry Pi entre otros.

## Características:
- Rango de medición de temperatura: -55° ~ + 125°.
- Precisión de rango: ± 0.5.
- Fuente de alimentación de trabajo: DC 3 ~ 5V.
- Los resultados de la medición en 9 ~ 12 transmisión en serie de cantidad digital.

## Pines:
- G (-) – Conectado a GND
- Y (S) – Conectado al pin 4
- R (+) – Conectado a (3.3V / 5V)

## Notas: 
* Para habilitar el pin 4 de la raspberry como 1-wire y poder
leer los datos de entrada, se tiene que ejecutar el comando:
```console
    sudo dtoverlay w1-gpio gpiopin=4 pullup=0
```
* Se debe instalar la libreria w1thermsensor en python con el comando
```console
    sudo pip3 install w1thermsensor
```
* Para ejecutarlo basta con ejecutar el comando
```console
   python3 temp-sensor.py
```