# Instrucciones de uso

./led-blink.sh gpio_pin_number times

>**gpio_pin_number** = Numero de pin en el cual está conectado el LED (De acuerdo a la distribucion GPIO.X) 

>**times** = Cantidad de veces que va a parpadear

Ejemplo: 
```
    ./led-blink.sh 14 5
```

Hará que el LED conectado al GPIO.14 parpadee 5 veces