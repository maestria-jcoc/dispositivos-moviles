#!/bin/bash

pin=$1
count=$2

gpio_path=/sys/class/gpio

if [[ $# -ne 2 ]]; then 
    echo "Usage: $0 gpio_pin_number times. Check README.md for further information"
    exit -1
fi

gpio_pin="$gpio_path/gpio$pin"

if [[ ! -d $gpio_pin ]]; then
    echo $pin > "$gpio_path/export"
fi
echo out > "$gpio_pin/direction"

for (( i = 0; i < count * 2; i++ )); do
    echo $(expr $i % 2) > "$gpio_pin/value"
    sleep 0.15
done

echo 0 > "$gpio_pin/value" # Apaga el LED al terminar la ejecucion

exit 0