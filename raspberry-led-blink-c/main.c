#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define GPIO "/sys/class/gpio"
#define LED "14"
#define LEDVALUE "/gpio14/value"
#define LEDDIRECTION "/gpio14/direction"
#define SLEEPTIME 100000

void writeLED(char[], char[]);
void setupLED();
void blinkLED(int);

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("Wrong number of arguments\n");
        return -1;
    }

    setupLED();

    if(strcmp(argv[1], "on") == 0) {
        printf("Turning ON LED\n");
        writeLED(LEDVALUE, "1");
    } else if(strcmp(argv[1], "off") == 0) {
        printf("Turning OFF LED\n");
        writeLED(LEDVALUE, "0");
    } else if(strcmp(argv[1], "blink") == 0) {
        printf("Blinking LED\n");
        blinkLED(5);
    }

    return 0;
}

void writeLED(char filename[], char value[]) {
    FILE* fp;
    char fullFile[100];
    sprintf(fullFile, GPIO "%s", filename);
    fp = fopen(fullFile, "w+");
    fprintf(fp, "%s", value);
    fclose(fp);
}

void setupLED() {
    writeLED("/unexport", LED);
    writeLED("/export", LED);
    writeLED(LEDDIRECTION, "out");
}

void blinkLED(int count) {
    for (int i = 0; i < count; i++)
    {
        writeLED(LEDVALUE, "1");
        usleep(SLEEPTIME);
        writeLED(LEDVALUE, "0");
        usleep(SLEEPTIME);
    }
    
}