const express = require("express")
const app = express()
const http = require("http")
const server = http.createServer(app)
const { Server } = require("socket.io")
const io = new Server(server)

app.get("/", (req, res) => {
	res.sendFile(__dirname + '/index.html')
})

io.on("connection", (socket) => {
	console.log("New client connected")
	socket.on('led', (data) => {
		if(data === "on"){
			socket.emit("ledStatus", "green")
		} else {
			socket.emit("ledStatus", "gray")
		}
	})
	socket.on('disconnect', () => {
		console.log("The user has disconnected")
	})
})

server.listen(3000, () => {
	console.log('Server listening on port: 3000')
})
