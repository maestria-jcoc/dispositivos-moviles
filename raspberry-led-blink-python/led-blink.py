#!/usr/bin/python3

import RPi.GPIO as gpio
import time
import sys, getopt

led = 8
times = 5
usageMessage = 'Usage: led-blink.py -p <gpio pin number> -t <blink times>'

def main(argv):
    global led, times
    try:
        opts, args = getopt.getopt(argv[1:], "hp:t:", ["help", "pin=", "times="])
        if len(opts) == 0:
            print(usageMessage)
            sys.exit(1)
    except getopt.GetoptError:
        print(usageMessage)
        sys.exit(2)
    for option, argument in opts:
        if option in ("-h", "--help"):
            print(usageMessage)
            sys.exit()
        elif option in ("-p", "--pin"):
            led = int(argument)
        elif option in ("-t", "--times"):
            times = int(argument)
    
    setup()
    blink_led()

def setup():
    gpio.setwarnings(False)
    gpio.setmode(gpio.BOARD)
    gpio.setup(led, gpio.OUT)

def blink_led():
    for i in range(times*2):
        gpio.output(led, (i % 2) == 0)
        time.sleep(0.15)

if __name__ == "__main__":
    main(sys.argv[1:])

