# Instrucciones de uso

python3 led-blink.py -p <gpio pin number> -t <blink times>

>**gpio pin number** = Numero de pin en el cual está conectado el LED (De acuerdo a la distribucion GPIO.Board) 
>**blink times** = Cantidad de veces que va a parpadear

>Si no se especifica el puerto ni el número de veces, se toma por defecto el puerto 8, y 5 veces el parpadeo

Ejemplo: 
```
    python3 led-blink.py -p 8 -t 5
```

Hará que el LED conectado al puerto (8) parpadee 5 veces

Ejemplo 2: 
```
    python3 led-blink.py --times=10
```

Hará que el LED conectado al puerto (8) parpadee 10 veces