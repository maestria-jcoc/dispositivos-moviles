# Raspberry Data Logger

Se conecta a un servidor de websockets para enviar continuamente
el estado del relevador y las variables de temperatura y humedad detectadas en el ambiente.

## Módulo sensor de humedad y temperatura DHT11

Estos sensores están compuestos en dos partes, un sensor de humedad capacitivo y un termistor, también constan de un circuito integrado básico en el interior que hace la conversión de analógico a digital y este envía una señal digital con la temperatura y la humedad.

### Caractersticas

- Alimentación de 3.3V a 5V DC
- Corriente máxima 2.5mA durante la conversión
- Lectura de humedad con un +/- 5% de precisión
- Lectura de temperatura con un +/- 2ºC de precisión
- Capaz de medir humedad de 20% a 90%
- Capaz de medir temperatura de 0 a 50ºC
- No más de 1Hz en velocidad de muestreo (una vez cada segundo)

[DHT11 Datasheet](https://www.mouser.com/datasheet/2/758/DHT11-Technical-Data-Sheet-Translated-Version-1143054.pdf)
[Node DHT11 Sensor Library](https://www.npmjs.com/package/node-dht-sensor)

### Pines:
- ( - ) – Conectado a GND
- ( S ) – Conectado al pin 14
- ( + ) – Conectado a (3.3V / 5V)

## Módulo relevador SRD-05VDC-SL-C
Relevador 5 volts SRD-05VDC-SL-C ampliamente utilizado en electrónica para proveer aislamiento y conmutar cargas de corriente alterna de hasta 10 Amperes.

La bobina de este relevador funciona con 5 volts, por lo que es adecuado si se desea alimentar desde la misma fuente que un arduino o un microcontrolador PIC que funciona con 5 volts.


### Características
- Corriente máxima: 10A 250VAC
- Resistencia de contacto: < = 100 m ( ohmios )
- Vida útil eléctrica: 100.000
- Vida mecánica: 10,000,000
- Voltaje de operación de la bobina: 5 VDC
- potencia de la bobina: 0.36W
- Aislamiento entre bobina y contactos: 1500V / min.
- Aislamiento contacto – contacto: 1000 VCA / min.

[SRD-05VDC-SL-C Datasheet](http://www.datasheet.es/download.php?id=720556)

### Notas

El script se ejecuta como un servicio en segundo plano con ayuda de pm2