const Gpio = require('onoff').Gpio;
const DHTSensor = require('node-dht-sensor').promises
const WebSocket = require('ws');

const RELAYPIN = 15         // Gpio 15
const DHTSENSORPIN = 14     // Gpio 14
const DHTSENSORTYPE = 11    // Sensor DHT11

const relay = new Gpio(RELAYPIN, 'out')

let timeOutID = null
const timeOut = 1000;
let pingTimeOutID = null;
const pingTimeOut = 10000    ; // 10 segundos

function toggleRelay() {
    if(relay.readSync() === 0)
        relay.writeSync(1)
    else
        relay.writeSync(0)
}

async function readTempAndHumidity() {
    try {
        const response = await DHTSensor.read(DHTSENSORTYPE, DHTSENSORPIN)
        const temp = response.temperature.toFixed(1)
        const humidity = response.humidity.toFixed(1)
        console.log(`Temp: ${temp}ºC, Humidity: ${humidity}%`)
        return {temp, humidity}
    } catch(err) {
        console.log('Error:', err)
    }
}

async function currentState() {
    try {
        const tempAndHumidity = await readTempAndHumidity()
	const res = {relay: relay.readSync(), temp: tempAndHumidity.temp, humidity: tempAndHumidity.humidity}
	console.log(res)
        return res
    } catch(err) {
        console.log('Error reading sensor: ', err)
	return {relay: undefined, temp: undefined, humidity: undefined}
    }
}

async function sendCurrentState(_socket) {
    const state = await currentState()
    const data = {sender_id: 'Raspberry_1', message: `Raspberry Current State`, state}
    _socket.send(JSON.stringify(data))
}

function startWebSocket() {
    clearInterval(timeOutID)

    const ws = new WebSocket('ws://websocket.nextik.mx:8090', {
        headers: {'user-agent': 'Raspberry Pi 4B'},
        perMessageDeflate: false
    })

    ws.on('open', async () => {
        console.log('Connected to websocket')
        clearInterval(pingTimeOutID)
        sendCurrentState(ws)

        pingTimeOutID = setInterval(() => {
            console.log('Sending ping')
            ws.ping()
        }, pingTimeOut)

        ws.on('close', () => {
            console.log('Socket disconnected')
            timeOutID = setInterval(() => {
                console.log('Reconnecting to socket...')
                startWebSocket()
            }, timeOut)
        })
    })

    ws.on('pong', () => {
        console.log('Received response from server. ')
    })

    ws.on('message', (data) => {
	    data = JSON.parse(data)
        //console.log("Mensaje recibido: ", data.message)
        if (data.message === 'Refresh' || data.message === 'New web client connected') {
	    console.log('Refreshing data')
            sendCurrentState(ws)
        }
        else if(data.message === 'Toggle Relay') {
            toggleRelay()
	        //console.log('Toggling relay', relay.readSync())
	        sendCurrentState(ws)
        }
    })

    ws.on('error', (error) => {
        console.log(`Unable to connect to websocket at: ${error.address}:${error.port} with code: ${error.code}`)
        timeOutID = setInterval(() => {
            console.log('Reconnecting to socket...')
            startWebSocket()
        }, timeOut)
    })
}

startWebSocket()
