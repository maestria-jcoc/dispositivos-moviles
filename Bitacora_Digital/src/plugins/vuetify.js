import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from 'vuetify/lib/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: {es},
        current: 'es',
    },
    theme: {
        themes: {
            light: {
                primary: '#118ab2',
                secondary: '#073b4c',
                success: '#06d6a0',
                warning: '#ffd166',
                error: '#ef476f',
            },
        }
    }
});
