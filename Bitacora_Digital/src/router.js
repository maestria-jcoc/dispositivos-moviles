import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const Dashboard = () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Dashboard')
const Login = () => import(/* webpackChunkName: "login" */ '@/views/Auth/Login')
const Home = () => import(/* webpackChunkName: "home" */  '@/views/Home')
const Samples = () => import(/* webpackChunkName: "samples" */ '@/views/Dashboard/Samples/Index')
const CreateSample = () => import(/* webpackChunkName: "samples" */ '@/views/Dashboard/Samples/Create')
const Laboratory = () => import(/* webpackChunkName: "laboratory" */ '@/views/Dashboard/Laboratory')
const NotFound = () => import(/* webpackChunkName: "notFound" */ '@/views/NotFound')

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {guest: true}
        },
        {
            path: '/',
            name: 'home',
            component: Home,
            redirect: '/dashboard',
            children: [
                {
                    path: 'dashboard',
                    name: 'dashboard',
                    component: Dashboard,
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: 'muestras/lista',
                    name: 'sample.index',
                    component: Samples,
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: 'muestras/nueva',
                    name: 'sample.create',
                    component: CreateSample,
                    meta: {
                        requiresAuth: true,
                        is_admin: true
                    }
                },
                {
                    path: 'muestras/:id',
                    name: 'sample.detail',
                    component: CreateSample,
                    meta: {
                        requiresAuth: true,
                        is_admin: true
                    }
                },
                {
                    path: 'laboratorio',
                    component: Laboratory,
                    name: 'laboratory',
                    meta: {
                        requiresAuth: true,
                        is_admin: true
                    }
                },
            ],
        },
        {path: '*', component: NotFound}
    ]
})
router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0)
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('jwt') == null) {
            next({
                path: '/login',
                params: {nextUrl: to.fullPath}
            })
        } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if (to.matched.some(record => record.meta.is_admin)) {
                if (user.is_admin === 1) {
                    next()
                } else {
                    next({name: 'dashboard'})
                }
            } else {
                next()
            }
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (localStorage.getItem('jwt') == null) {
            next()
        } else {
            next({name: 'dashboardx'})
        }
    } else {
        next()
    }
})

export default router