# Bitacora digital

El proyecto consta de varias partes:

* El backend es un servidor web+websockets desarrollado con nodejs y express contenido en la carpeta "server"
* El frontend está hecho en el framework VueJS utilizando las librerias de componentes visuales de Vuetify

## Configuración del proyecto

Se necesitan instalar las dependencias del proyecto con el comando

```shell
npm install
```

### Desarrollo del proyecto

Para comenzar a desarrollar el proyecto es necesaria la ejecución de ambas partes del mismo, el servidor web y los
websockets, se ejecutan con el siguiente comando:

```shell
node server/app.js
```

Lo cual genera una instancia del servidor en el puerto 8090, es necesario volver a ejecutar el comando cuando se
realicen cambios a los archivos del servidor.

Para el frontend es necesario ejecutar el comando:

```shell
npm run serve
```

Que generará una instancia del frontend accesible desde el puerto 8000, esta instancia se recarga de forma automática al
detectar un cambios en los archivos del proyecto.

### Compila y minifica los archivos para producción

Con este comando se crea la carpeta "dist" necesaria para hacer el despliegue en producción del sistema

```shell
npm run build
```

## Notas importantes

Este proyecto se complementa con otro contenido en este mismo repositorio:

* Para conectar el cliente Raspberry, es necesario seguir las instrucciones del
  proyecto: [raspberry_datalogger_nodejs](https://gitlab.com/maestria-jcoc/dispositivos-moviles/-/tree/master/raspberry_datalogger_nodejs)
  .


* Para conectar el cliente de ESP32 es necesario seguir las instrucciones del
  proyecto: [esp32_socket](https://gitlab.com/maestria-jcoc/virtual-instrumentation/-/tree/master/esp32_socket)

## Links útiles

* [Vuejs](https://vuejs.org)
* [Vuetify](https://vuetifyjs.com/en/)