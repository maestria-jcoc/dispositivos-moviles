"use strict";

const mysql = require('mysql')

class Db {

    constructor(_host = 'localhost', _user = 'root', _password = '12345', _database = null) {
        this.connection = mysql.createConnection({
            host: _host,
            user: _user,
            password: _password,
            database: _database
        })
    }

    createSample(form, callback) {
        return this.connection.query(`INSERT INTO samples (dia_fin,
                                                           dia_inicio,
                                                           fase,
                                                           hora_fin,
                                                           hora_inicio,
                                                           name,
                                                           observaciones,
                                                           relacion_molar,
                                                           tecnicas,
                                                           temp_ttermico,
                                                           tiempo_lavado,
                                                           tiempo_secado,
                                                           tiempo_ttermico)
                                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, form, (err, result) => {
            callback(err, result)
        })
    }

    deleteSample(id, callback) {
        return this.connection.query(`DELETE
                                      FROM samples
                                      WHERE id = ?`, [id], (err, result) => {
            callback(err, result)
        })
    }

    getSamples(callback) {
        return this.connection.query(`SELECT *
                                      FROM samples`, (err, result) => {
            callback(err, result)
        })
    }

    getSampleById(id, callback) {
        return this.connection.query(`SELECT id,
                                             name,
                                             relacion_molar,
                                             temp_ttermico,
                                             tiempo_ttermico,
                                             tiempo_secado,
                                             tiempo_lavado,
                                             fase,
                                             tecnicas,
                                             observaciones,
                                             created_at,
                                             TIME_FORMAT(hora_inicio, '%h:%i:%s') as hora_inicio,
                                             TIME_FORMAT(hora_fin, '%h:%i:%s')    as hora_fin,
                                             DATE_FORMAT(dia_inicio, '%Y-%m-%d')  as dia_inicio,
                                             DATE_FORMAT(dia_fin, '%Y-%m-%d')     as dia_fin
                                      FROM samples
                                      WHERE id = ?`, [id], (err, sample) => {
            callback(err, sample[0])
        })
    }

    getLogsByDateRange(data, callback) {
        const inicio = data.dia_inicio + ' ' + data.hora_inicio
        const fin = data.dia_fin + ' ' + data.hora_fin

        return this.connection.query(`SELECT *
                                      FROM status_logs
                                      WHERE timestamp BETWEEN ? AND ?`, [inicio, fin], (err, logEntries) => {
            callback(err, logEntries)
        })
    }

    insertRelayLog(data, callback) {
        this.connection.query(`INSERT INTO relay_logs (user_id, action, status, controlador)
                               values (?, ?, ?, ?);`, [data.user_id, data.message, data.state, data.to], (err, logEntry) => {
            callback(err, logEntry)
        })
    }

    insertStatusLog(data, callback) {
        this.connection.query(`INSERT INTO status_logs (relay, tempC, tempF, humidity, microController)
                               values (?, ?, ?, ?, ?);`, data, (err, logEntry) => {
            callback(err, logEntry)
        })
    }

    selectByUser(user, callback) {
        return this.connection.query(`SELECT *
                                      FROM users
                                      WHERE user = ?`, [user], (err, result) => {
            callback(err, result[0])
        })
    }

    updateSampleById(id, form, callback) {
        return this.connection.query(`UPDATE samples
                                      SET name            = ?,
                                          relacion_molar  = ?,
                                          temp_ttermico   = ?,
                                          tiempo_ttermico = ?,
                                          tiempo_secado   = ?,
                                          tiempo_lavado   = ?,
                                          fase            = ?,
                                          tecnicas        = ?,
                                          observaciones   = ?,
                                          hora_inicio     = ?,
                                          hora_fin        = ?,
                                          dia_inicio      = ?,
                                          dia_fin         = ?
                                      WHERE id = ?`, form.concat(id), (err, sample) => {
            callback(err, sample)
        })
    }
}

module.exports = Db