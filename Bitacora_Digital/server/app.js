"use strict"
const express = require('express')
const http = require('http')
const WebSocket = require('ws').Server
const app = express()
const apiRoutes = require('./api')
const DB = require('./db')
const path = require('path');
const history = require('connect-history-api-fallback');

const db = new DB("localhost", "root", "12345", "esp32_websocket")
const server = http.createServer(app)
const wss = new WebSocket({server: server, path: '/'})
const clients = new Set()

wss.ping = () => {
    clients.forEach((client) => {
        if (client.socket.readyState === 1)
            client.socket.ping();
        else
            console.log('client disconnected: ', client.client_type)
    })
}
wss.broadcast = (data, sender = null, client_type = null) => {
    // console.log(`Broadcasting message to: ${client_type ? client_type : 'all'} --> ${data.message}`)
    clients.forEach((client) => {
        if (client.ws !== sender && client.ws.readyState === 1) {
            if (!client_type || client_type === client.client_type) {
                client.ws.send(JSON.stringify(data))
            }
        }
    })
}
wss.on('connection', (ws, req) => {
    let client_type = undefined
    const userAgent = req.headers['user-agent']
    try {
        client_type = userAgent.includes('Arduino') ? 'arduino' : (userAgent.includes('Raspberry') ? 'raspberry' : 'web')
    } catch (err) {
        console.log('No user agent present in headers: ', req.headers)
    }
    clients.add({ws, client_type})

    ws.send(JSON.stringify({sender_id: 'Server', message: "Bienvenido al servidor de sockets"}))
    wss.broadcast({sender_id: 'Server', message: `New ${client_type} client connected`}, ws)
    ws.on('close', () => {
        clients.forEach(client => client.socket === ws ? clients.delete(client) : client)
        //console.log(`${client_type} client disconnected Clients: ${clients.size}`)
        ws.terminate()
    })
    ws.on('message', (msg) => {
        const data = JSON.parse(msg)
        if (data.sender_id === 'Raspberry_1' || data.sender_id === 'ESP32_1') {
            // If message comes from Raspberry or ESP32
            wss.broadcast(data, ws, 'web')
            if (data.message.includes('Current State')) {
                let currentStatus = []
                if (data.sender_id === 'Raspberry_1') {
                    if (data.state.temp === undefined) return
                    currentStatus = [
                        data.state.relay,
                        parseFloat(data.state.temp).toFixed(1),
                        parseFloat((data.state.temp * 1.8) + 32).toFixed(1),
                        parseFloat(data.state.humidity).toFixed(1),
                        data.sender_id
                    ]
                } else if (data.sender_id === 'ESP32_1') {
                    currentStatus = [
                        data.pinOutStatus.relay,
                        parseFloat(data.pinOutStatus.tempC).toFixed(1),
                        parseFloat(data.pinOutStatus.tempF).toFixed(1),
                        null,
                        data.sender_id
                    ]
                }

                db.insertStatusLog(currentStatus, (err, logEntry) => {
                    if (err) ws.send(JSON.stringify({
                        sender_id: 'Server',
                        message: `Error en el servidor: ${err}.`
                    }))
                    if (!logEntry) ws.send(JSON.stringify({
                        sender_id: 'Server',
                        message: `Error al guardar la entrada en el registro.`
                    }))
                })
            }
        } else {
            if (data.message !== 'Refresh') {
                db.insertRelayLog(data, (err, logEntry) => {
                    if (err) ws.send(JSON.stringify({
                        sender_id: 'Server',
                        message: `Error en el servidor: ${err}.`
                    }))
                    if (!logEntry) ws.send(JSON.stringify({
                        sender_id: 'Server',
                        message: `Error al guardar la entrada en el registro.`
                    }))
                })
            }
            // If a message comes from webclients
            wss.broadcast(data, ws, data.to)
        }
    })
})

// CORS middleware
const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', '*')
    res.header('Access-Control-Allow-Headers', '*')
    next();
}

app.use(allowCrossDomain)
app.use('/api', apiRoutes)
app.use(history());
app.use(express.static(path.join(__dirname, '../dist')))

let port = Number(process.env.PORT) || 8090;
server.listen(port, () => {
    console.log(`Server started on port ${server.address().port} :)`);
});